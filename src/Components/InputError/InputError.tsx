import { faExclamationCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import styles from "./InputError.module.css";

type Props = {
  children: React.ReactNode,
};

const InputError = ({ children }: Props) => {
    if(!children) {
        return null;
    }
    return (
        <span className={ styles.inputError }>
            <FontAwesomeIcon icon={ faExclamationCircle } style={ { marginRight: "5px" } }/>
            { children }
        </span>
    );
};


export default InputError;
