import cx from "classnames";
import InputError from "Components/InputError/InputError";

import React, { FocusEventHandler } from "react";
import styles from "./TextInput.module.css";

export interface FieldState {
    invalid: boolean;
    isTouched: boolean;
    isDirty: boolean;
    error?: { message?: string };
}
  
export interface FieldProps {
    onChange: Function;
    value: any;
    name: string;
    ref: any;
}
  

interface TextInputProps {
  children?: React.ReactNode,
  className?: string,
  inputClass?: string,
  label: string,
  optional?: boolean,
  type?: string,
  onFocus?: FocusEventHandler,
  onKeyDown?: Function,
  normalize?: Function,
  fieldProps: FieldProps,
  fieldState: FieldState,
  placeholder?: string,
  inputMode?: "text" | "search" | "none" | "tel" | "url" | "email" | "numeric" | "decimal",
  maxLength?: number,
  name?: string,
}

const TextInput: React.VFC<TextInputProps> = (props: TextInputProps) => {
    const {
        className,
        inputClass,
        type,
        label,
        children,
        onFocus,
        fieldState,
        fieldProps,
        normalize,
        onKeyDown,
        placeholder,
        inputMode,
        name
    } = props;
    const { isTouched, error } = fieldState;
    const showError = isTouched && error;
    const hasValue = !!fieldProps.value;

    const onFocusProp = onFocus ? { onFocus } : {};
    const onKeyDownProp = onKeyDown ? { onKeyDown } : {};

    const handleChange = (e) => {
        const val = e && e.target ? e.target.value : e;
        fieldProps.onChange(normalize ? normalize(val) : val);
    };

    return (
        <div
            className={ cx(styles.fieldWrapper, {
                [styles.fieldWrapperFilled]: hasValue
            }, className) }
        >
            <div className={ styles.inputWrapper }>
                { /* @ts-ignore */ }
                <input
                    onFocus={ onFocus }
                    { ...fieldProps }
                    { ...onFocusProp }
                    { ...onKeyDownProp }
                    placeholder={ placeholder }
                    inputMode={ inputMode }
                    type={ type }
                    className={ cx(styles.input, {
                        [styles.inputError]: showError
                    }, inputClass) }
                    onChange={ handleChange }
                    name={ name }
                />
                <div
                    className={ cx(styles.label, {
                        [styles.labelFilled]: hasValue,
                        [styles.labelError]: showError
                    }) }
                >
                    { label }
                </div> 
            </div>
            { showError && (
                <InputError>
                    { error?.message?.toString() }
                </InputError>
            ) }
            { children }
        </div>
    );
};

TextInput.defaultProps = {
    children: null,
    className: "",
    inputClass: "",
    inputMode: undefined,
    maxLength: undefined,
    name: "",
    normalize: undefined,
    onFocus: undefined,
    onKeyDown: undefined,
    optional: false,
    placeholder: undefined,
    type: "text"
};

export default TextInput;
