import InputError from "Components/InputError/InputError";
import { NameForm } from "Modules/FormSection/FormSection";
import React from "react";
import { Control, Controller } from "react-hook-form";
import styles from "./LongTextInput.module.css";

interface Props {
  disabled?: boolean,
  className?: string,
  control: Control<NameForm, any>
}

const LongTextInput = (props: Props) => {
    return (
        <Controller
            control={ props.control }
            defaultValue={ "" }
            name="longText"
            render={ ({ field: { value, onChange }, fieldState: { error } }) => (
                <div className={ styles.container }>
                    <textarea
                        value={ value }
                        onChange={ onChange }
                        rows={ 10 }
                        cols={ 40 }
                        maxLength={ 100 }
                        className={ styles.textarea }
                    />
                    { value.length > 0 && (`Characters ${value.length}`) } 
                    <InputError>
                        { error?.message?.toString() }
                    </InputError>
                </div>
            ) }
        />

    );
};


export default LongTextInput;
