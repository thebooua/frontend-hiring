import cx from "classnames";
import * as React from "react";
import styles from "./Button.module.css";

type Props = {
  children: React.ReactNode,
  disabled?: boolean,
  onClick: Function,
  className?: string,
  type?: "submit" | "button",
};

const Button = ({
    children,
    disabled,
    onClick,
    className = "",
    type,
    ...props
}: Props) => {
    const typeProps = type ? { type } : {};
    return (
        <button
            type="submit"
            className={ cx(styles.button, className,
                {
                    [styles.disabledButton]: disabled
                }) }
            disabled={ disabled }
            onClick={ onClick as React.MouseEventHandler<HTMLButtonElement> }
            data-title={ type }
            { ...typeProps }
            { ...props }
        >
            { children }
        </button>
    );
};

Button.defaultProps = {
    className: "",
    disabled: false,
    style: {},
    type: "submit"
};

export default Button;
