import React from "react";
import styles from "./Section.module.css";

type SectionProps = {
  children: React.ReactChild,
  title: string,
}

const Section: React.FC<SectionProps> = (props) => {
    const { children, title } = props;
    return (
        <section className={ styles.section }>
            <h1>{ title }</h1>
            { children }
        </section>
    );
};

export default Section;
