import axios from "axios";
import cx from "classnames";
import InputError from "Components/InputError/InputError";
import useUpdateEffect from "hooks/useUpdateEffect";
import { NameForm } from "Modules/FormSection/FormSection";
import React, { useEffect, useState } from "react";
import { Control, Controller } from "react-hook-form";
import Select from "react-select";
import styles from "./SelectInput.module.css";

interface Props {
  disabled?: boolean,
  className?: string,
  control: Control<NameForm, any>
}

const SelectInput = ({  disabled, className = "", control }: Props) => {
    const [options, setOptions] = useState([]);
    const [data, setData] = useState(null);

    useEffect(() => {
        const fetchData = async() => {
            const response = await axios.get("https://countriesnow.space/api/v0.1/countries/iso");
            if (response?.data?.error != "") {
                return null;
            }
            const data = response?.data?.data;
            setData(data);
        };
        fetchData();
    }, []);
    
    useUpdateEffect(() => {
        if(data) {
            setOptions(data?.map(({ name }) => {
                return {
                    label: name,
                    value: name?.toLowerCase()
                };
            }));
        }
    }, [data]);
    return (
        <Controller
            control={ control }
            defaultValue={ "" }
            name="country"
            render={ ({ field: { value, onChange }, fieldState: { error } }) => (
                <div className={ styles.container }> 
                    <Select
                        className={ cx(styles.select, className, { [styles.selectDisabled]: disabled }) }  
                        options={ options }
                        value={ options.find(c => c.value === value) }
                        onChange={ val => onChange(val.value) }
                    />
                    <InputError>
                        { error?.message?.toString() }
                    </InputError>
                </div>
            ) }
        />

    );
};

SelectInput.defaultProps = {
    className: "",
    disabled: false,
    style: {},
    type: "submit"
    
};

export default SelectInput;
