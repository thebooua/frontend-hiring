import cx from "classnames";
import React from "react";
import styles from "./RadioButton.module.css";

interface RadioButtonProps {
  label: React.ReactNode,
  selected: unknown,
  handleRadioChange: Function,
  error?: boolean,
  selectedValue: any,
  className?: string,
  labelClassName?: string,
}

const RadioButton = ({
    label,
    selectedValue,
    selected,
    handleRadioChange,
    error,
    className,
    labelClassName
}: RadioButtonProps) => {
    return (
        <button
            className={ cx(styles.button, className) }
            type="button"
            onClick={ () => {
                handleRadioChange(selectedValue);
            } }
        >
            <span className={ cx(styles.label, labelClassName) }>
                { label }
            </span>
            <div
                className={ cx(styles.checkContainer, {
                    [styles.checkContainerFilled]: selected === selectedValue,
                    [styles.buttonError]: error,
                    [styles.checkLongerLabel]: typeof label === "string" ? label.length > 10 : false
                }) }
            >
                { selected === selectedValue && (
                    <div
                        className={ styles.checkFilled }
                    />
                ) }
            </div>
        </button>
    );
};

RadioButton.defaultProps = {
    className: "",
    error: false,
    labelClassName: ""
};

export default RadioButton;
