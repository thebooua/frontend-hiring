import InputError from "Components/InputError/InputError";
import React, { useEffect, useState } from "react";
import { getBase64FromImage } from "./helpers";
import styles from "./ImageInput.module.css";


export interface FieldState {
    invalid: boolean;
    isTouched: boolean;
    isDirty: boolean;
    error?: { message?: string };
  }
  
export interface FieldProps {
    onChange: Function;
    value: any;
    name: string;
  }
  

interface Props {
    fieldState: FieldState, 
    imageFieldProps: FieldProps,
    pathFieldProps: FieldProps,
    fieldSetValue: Function,
}

  
export const ImageInput = (props: Props) => {
    const { fieldState, imageFieldProps, pathFieldProps, fieldSetValue } = props;

    const { error, isTouched } = fieldState;
    const showError = isTouched && error;
    const [images, setImages] = useState([]);
    const [imageUrls, setImageUrls] = useState([]);

    const onImageChange = async(e) => {
        const image = e.target.files[0];
        setImages([...e.target.files]);
        pathFieldProps.onChange(e);
        fieldSetValue("image", await getBase64Img(image));
    };

    const getBase64Img = async(image) => {
        try {
            const result = await getBase64FromImage(image);
            return result;
        } catch (error) {
            // eslint-disable-next-line no-console
            console.error(error);
            return null;
        }
    };

    useEffect(() => {
        if ( images?.length < 1) return;
        const newImgUrls = [];
        images?.forEach(image => newImgUrls.push(URL.createObjectURL(image)));
        setImageUrls(newImgUrls);
    }, [images]);
  

    return (
        <div className={ styles.container }>
            <div> 
                <label htmlFor="imagePath" className={ styles.label }>Select an image</label>
                <input 
                    { ...pathFieldProps }
                    type="file" 
                    id="imagePath"
                    className={ styles.file }
                    accept="image/*" 
                    onChange={ onImageChange } />
                
            </div>
            { /* @ts-ignore */ }
            <input 
                { ...imageFieldProps }
                type="hidden" />
            <div className={ styles.imageContainer }>
                { imageUrls.map(imageSrc => <img width={ 300 } key={ imageSrc } src={ imageSrc } alt={ imageSrc }/>) }
            </div>
            <InputError>
                { showError && error?.message?.toString() }
            </InputError>
        </div>
    );
};
