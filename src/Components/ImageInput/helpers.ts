export const getBase64FromImage = image => {
    return new Promise(resolve => {
        let baseURL;
        let reader = new FileReader();
        reader.readAsDataURL(image);
        reader.onload = () => {
            baseURL = reader.result;
            resolve(baseURL);
        };
    });
};
