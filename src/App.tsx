import { faSun } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "App.module.css";
import cx from "classnames";
import FormSection from "Modules/FormSection/FormSection";
import SubmissionsSection from "Modules/SubmissionsSection/SubmissionsSection";
import React, { useState } from "react";

const App = () => {
    const [isDarkTheme, setIsDarkTheme] = useState(false);
    const toggleTheme = () => {
        setIsDarkTheme(!isDarkTheme);
    };

    return (
        <div 
            className={ cx(styles.appContainer, {
                [styles.appContainerDarkTheme]: isDarkTheme
            }) }
        >
            <h1 className={ styles.appTitle }>Project questionnaire</h1>
            <FontAwesomeIcon icon={ faSun } className={ styles.themeIcon } onClick={ toggleTheme }/>
            <FormSection />
            <SubmissionsSection/>
        </div>
    );
};

export default App;
