import Section from "Components/Section/Section";
import { useLiveQuery } from "dexie-react-hooks";
import { db } from "helpers/db";
import { questionList } from "Modules/FormSection/helpers/optionsList";
import React from "react";
import { capitalizeFirstLetter } from "./helpers/helpers";
import styles from "./SubmissionsSection.module.css";

const SubmissionsSection = () => {
    
    const formData = useLiveQuery(() => db?.formData?.toArray());
    return (
        <Section title="Previous Submissions">
            <>
                { formData?.map((submission) => {
                    return (
                        <div key={ submission?.id } className={ styles.submission }>
                            { submission?.id }
                            { " " }
                            <img width={ 100 } src={ submission?.image } alt={ submission?.name } />
                            { submission?.name }
                            { " " }
                            { submission?.birthdate }
                            { " " }
                            { capitalizeFirstLetter(submission?.country) }
                            { " " }
                            { questionList.find(({ value }) => value === submission?.question)?.label } 
                            { " " }
                            { submission?.longText }
                        </div>
                    );
                }) }
            </>
        </Section>
    );
};

export default SubmissionsSection;
