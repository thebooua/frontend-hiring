import Button from "Components/Button/Button";
import { ImageInput } from "Components/ImageInput/ImageInput";
import InputError from "Components/InputError/InputError";
import LongTextInput from "Components/LongTextInput/LongTextInput";
import RadioButton from "Components/RadioButton/RadioButton";
import Section from "Components/Section/Section";
import SelectInput from "Components/SelectInput/SelectInput";
import TextInput from "Components/TextInput/TextInput";
import { addFormDataToDb } from "helpers/db";
import React, { useEffect, useRef, useState } from "react";
import { Controller, useController, useForm } from "react-hook-form";
import styles from "./FormSection.module.css";
import { questionList } from "./helpers/optionsList";
import { FormResolvers, normalizeDate, normalizeName } from "./helpers/validators";

export interface NameForm {
  name: string;
  birthdate: string;
  country: string,
  question: string,
  longText: string,
  imagePath: any,
  image: any,
}

const FormSection = () => {
    const [submitMessage, setSubmitMessage] = useState(null);
    const timeoutRef = useRef();

    const { handleSubmit, control, setFocus, setValue,  formState: { errors, isValid } } = useForm<NameForm>({
        mode: "onChange",
        resolver: FormResolvers.QuestionnareForm
    });

    const { field: nameFieldProps, fieldState: nameFieldState } = useController({
        control,  
        name: "name"
    });

    const { field: bdayFieldProps, fieldState: bdayFieldState } = useController({
        control,
        name: "birthdate"
    });

    const { field: imagePathFieldProps, fieldState: imagePathFieldState } = useController({
        control,
        name: "imagePath"
    });

    const { field: imageFieldProps } = useController({
        control,
        name: "image"
    });

    useEffect(() => {
        const timeout = timeoutRef.current;
        return () => {
            clearTimeout(timeout);
        };
    }, []);


    const onSubmit = async data => {
        const id = await addFormDataToDb(data);
        setSubmitMessage(`Saved entry no. ${id}`);
        
        const timeout: any = setTimeout(() => {
            setSubmitMessage(null);
        }, 2000);

        timeoutRef.current = timeout;
    };

    

    useEffect(() => {
        setFocus("name");
    }, [setFocus]);

    return (
        <Section title="Form Section">
            <div className={ styles.container }>
                <form onSubmit={ handleSubmit(onSubmit) }>
                    <h3> What&apos;s your name? </h3>
                    <TextInput 
                        name="name"
                        data-testid="name"
                        label="Your full name"
                        normalize={ normalizeName }
                        className={ styles.nameField }
                        fieldState={ nameFieldState }
                        fieldProps={ nameFieldProps }/>
                    <h3> What&apos;s your birthdate? </h3>
                    <div className={ styles.birthdateField }>
                        <TextInput
                            name="birthdate"
                            data-testid="birthdate"
                            label="Your Birthdate"
                            inputMode="numeric"
                            normalize={ normalizeDate }
                            className={ styles.birthdateField }
                            fieldState={ bdayFieldState }
                            fieldProps={ bdayFieldProps }
                        />
                        <InputError>
                            { errors?.birthdate?.message?.toString() }
                        </InputError>
                    </div>
                    <h3> Where you&apos;re from? </h3>
                    <SelectInput control={ control } className={ styles.selectField }/>
                    <h3> Please upload your project logo: </h3>
                    <div className={ styles.imageField }>
                        <ImageInput 
                            fieldSetValue = { setValue }
                            fieldState={ imagePathFieldState }
                            pathFieldProps={ imagePathFieldProps }
                            imageFieldProps={ imageFieldProps }
                        />
                        <InputError>
                            { errors?.imagePath?.message?.toString() }
                        </InputError>
                    </div>
                    <h3> Do you want to have a dark mode feature in your app?</h3>
                    <div className={ styles.radioGroup }>
                        { questionList.map((answer) => (
                            <Controller
                                key={ answer.value }
                                control={ control }
                                defaultValue={ "" }
                                name="question"
                                render={ ({ field: { value, onChange } }) => (
                                    <div className={ styles.column }>
                                        <RadioButton
                                            selectedValue={ answer.value }
                                            handleRadioChange={ onChange }
                                            selected={ value }
                                            className={ styles.radioButton }
                                            label={ answer.label }  
                                        />
                                    </div>                                
                                ) }
                            />
                        )) } 
                    </div>
                    <InputError>
                        { errors?.question?.message?.toString() }
                    </InputError>
                    <h3>Please provide a short project description - max: 1000 characters</h3>
                    <LongTextInput control={ control } />
                    <Button 
                        type="submit" 
                        onClick={ null }
                        disabled={ !isValid }
                        className={ styles.submitBtn } >
                        <span style={ { textAlign: "center" , width: "100%" } }>
                            Submit
                        </span>
                    </Button>
                    { submitMessage && submitMessage }
                </form>
            </div>
        </Section>
    );
};

export default FormSection;
