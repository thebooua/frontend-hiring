import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { validateDate } from "./dates";
import { questionList } from "./optionsList";

const NAME_REGEX = /^[a-z '-]+$/i;

export const normalizeName = (name: string) => name.charAt(0).toUpperCase() + name.slice(1);

export const normalizeDate = (value: string, previousValue: string) => {
    if (!value) {
        return value;
    }
    const onlyNums = value.replace(/[^\d]/g, "");
  
    if (!previousValue || value.length > previousValue.length) {
        if (onlyNums.length === 2) {
            return `${onlyNums}/`;
        }
        if (onlyNums.length === 4) {
            return `${onlyNums.slice(0, 2)}/${onlyNums.slice(2)}/`;
        }
    }
  
    if (value.length < previousValue?.length) {
        return value;
    }
  
    if (onlyNums.length <= 2) {
        return onlyNums;
    }
    if (onlyNums.length <= 4) {
        return `${onlyNums.slice(0, 2)}/${onlyNums.slice(2)}`;
    }
    return `${onlyNums.slice(0, 2)}/${onlyNums.slice(2, 4)}/${onlyNums.slice(4, 8)}`;
};

export const QuestionnareFormSchema = yup.object().shape({
    birthdate: yup.mixed()
        .test("dateLength", "Date is too short.", (dob: string) => dob?.length === 10)
        .test("dateAge", "Not a valid birthdate.", (dob: string) => validateDate(dob || "", "age"))
        .test("dateFormat", "Please enter valid date format (MM/DD/YYYY).", 
            (dob: string) => validateDate(dob || "", "format"))
        .required("Date is required"),
    country: yup.string()
        .required("Country is required"),
    image: yup.mixed()
        .required("Image is required"),
    imagePath: yup.mixed()
        .required("Image is required"),
    longText: yup.string()
        .max(100, "max 100 characters")
        .required("Description is required"),
    name: yup.string()
        .min(2, "First name should contain at least two chars.")
        .matches(NAME_REGEX, "First name should not contain special characters.")
        .required("Name is required"),
    question: yup.string()
        .test("question", "You need to select question from available values",
            (question: string) => questionList.map(({ value }) => value).includes(question))
        .required("Question is required")

}).required();

export const FormResolvers = {
    QuestionnareForm: yupResolver(QuestionnareFormSchema)
};
