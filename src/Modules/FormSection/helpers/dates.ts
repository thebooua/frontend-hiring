import dayjs from "dayjs";

export const DateFormat = {
    "MM/DD/YYYY": "MM/DD/YYYY",
    "YYYY-MM-DD": "YYYY-MM-DD"
};

const isValidDate = (date: string, dateFormat: string, strict = true) => date 
 && dayjs(`${date}`, dateFormat, strict).isValid();

export const getTimeDifferenceSinceNow = (date: string, 
    unit: "years" | "hours" | "days" | "minutes"
) => (date ? dayjs().diff(dayjs(`${date}`), unit) : null);

export const validateDate = (value: string, type: string) => {
    if (type === "format") {
        if (value.length === 10) {
            return isValidDate(value, DateFormat["MM/DD/YYYY"], true);
        }
    } else if (type === "age") {
        const yearsSince = getTimeDifferenceSinceNow(value, "years");
        return yearsSince <= 120 && yearsSince >= 15;
    }
  
    return true;
};
