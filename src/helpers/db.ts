import Dexie, { Table } from "dexie";

export interface FormData {
  id?: number;
  name: string;
  birthdate: string;
  country: string,
  question: string,
  longText: string,
  imagePath: any,
  image: any,
}


export async function addFormDataToDb(data) {
    try {
        const id = await db.formData.add({
            ...data
        });
        return id;
    } catch (error) {
        console.log(error);  
        return null;
    }
}


export class MySubClassedDexie extends Dexie {
  formData: Table<FormData>; 

  constructor() {
      super("myDatabase");
      this.version(1).stores({
          formData: "++id, name, birthdate, country, question, longText, imagePath, image"
      });
  }
}

export const db = new MySubClassedDexie();
