describe("E2E questionaire test", () => {
    it("successfully loads", () => {
        cy.visit("http://localhost:3000");
    });

    it("check for errors in name input", () => {
        cy.get("input[name=name]").type("test 123");
        cy.get("body").click(0,0);
        cy.get("*[class^=\"InputError\"]").should("contain", "First name should not contain special characters.");
        cy.get("input[name=name]").clear();
        cy.get("input[name=name]").type("testing user");
    });

    it("check for errors in birthdate", () => {
        cy.get("input[name=birthdate]").type("99999999");
        cy.get("*[class^=\"InputError\"]").should("contain", "Not a valid birthdate.");
        cy.get("input[name=birthdate]").clear();
        cy.get("input[name=birthdate]").type("10102000");
    });

    it("select country", () => {
        cy.get("*[class^=\"SelectInput\"]").find("*[class^=\"FormSection\"]").click().type("Poland");
        cy.get("*[class^=\"SelectInput\"]").find("*[class^=\"FormSection\"]").find("*[class*=\"menu\"]").find("*[class*=\"MenuList\"]").contains("Poland").click();
    });

    it("Testing picture uploading", () => {
        const fixtureFile = "test.jpg";
        cy.get("input[type=\"file\"").attachFile(fixtureFile);
    });

    it("go through checkboxes", () => {
        cy.get("*[class^=\"RadioButton\"]").click({ multiple: true });
    });

    it("type a description", () => {
        cy.get("*[class^=\"LongTextInput\"]").find("textarea").type("test");
    });

    it("check if form is ready for submission", () => {
        cy.get("button[type=submit]").should("not.be.disabled");
    });

    it("submit Form", () => {
        cy.get("button[type=submit]").click();
    });

    it("check if submission was saved", () => {
        cy.get("*[class^=\"SubmissionsSection\"]");
    });
});
